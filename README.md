# quick-form

## Overview


## Download


### CDN
_in process_

## Options

### wrapperClass

#### tooltipClass
#### messages
#### clear
(_default: true_)
Boolean flag indicating whether the form should be cleared if the submit is successful
#### privacyCheckbox
(_default: false_)
description
```javascript
privacyCheckbox: '#checkbox-1',
```
#### submitButton
(_default: [type="submit"]_)
description
```javascript
submitButton: '.form__button', 
```
#### validators
#### validate
#### beforeSend
#### error
#### success
#### rejected
